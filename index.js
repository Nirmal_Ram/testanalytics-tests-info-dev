"use strict";
const dbjs = require("./db.js");
exports.handler = async (event) => {
  const db = await dbjs.get();

  const data = await db
    .collection("hotstar-testAnalytics")
    .findOne({ "uuid._id": event.params.path.testUUID }, { uuid: 1, info: 1 });

  let total = 0;
  let passed = 0;
  let failed = 0;
  if (data.info.testCases.length != 0) {
    total = data.info.testCaseSummary.total;
    passed = data.info.testCaseSummary.passed;
    failed = data.info.testCaseSummary.failed;
  }
  let result = {
    version: "1.2",
    uuid: {
      _id: data.uuid._id,
      testId: data.uuid.testId,
      sessionId: data.uuid.testId,
      userDisplayName: data.uuid.userDisplayName,
      userEmail: data.uuid.userEmail,
      username: data.uuid.username,
    },
    testStartTime: data.info.testStartTime,
    projectName: data.info.projectName,
    appVersion: data.info.appVersion,
    scriptName: data.info.scriptName,
    deviceName: data.info.deviceName,
    deviceOSVersion: data.info.deviceOSVersion,
    deviceLocation: data.info.deviceLocation,
    deviceNetwork: data.info.deviceNetwork,
    testStatus: data.info.testStatus,
    testCaseSummary: {
      total: total,
      passed: passed,
      failed: failed,
    },
    testConfiguration: {
      captureHAR: data.info.testConfiguration.captureHAR,
      captureCPUMetrics: data.info.testConfiguration.captureCPUMetrics,
      captureMemoryMetrics: data.info.testConfiguration.captureMemoryMetrics,
      captureBatteryMetrics: data.info.testConfiguration.captureBatteryMetrics,
      captureGraphicsMetrics:
        data.info.testConfiguration.captureGraphicsMetrics,
      captureHeapMemoryMetrics:
        data.info.testConfiguration.captureHeapMemoryMetrics,
      captureDeviceSreenShots:
        data.info.testConfiguration.captureDeviceSreenShots,
      recordDeviceSreen: data.info.testConfiguration.recordDeviceSreen,
      captureDeviceNetworkPackets:
        data.info.testConfiguration.captureDeviceNetworkPackets,
    },
  };

  const response = {
    statusCode: 200,
    body: result,
    headers: {
      "Access-Control-Allow-Headers":
        "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
    },
  };
  return response;
};
